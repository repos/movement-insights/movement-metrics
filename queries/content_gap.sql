WITH quality_article_counts AS (
    SELECT
        TO_DATE(c.time_bucket) AS month,
        c.content_gap,
        IF(
            category IN (
                'male', 'cisgender male', 'Central & Eastern Europe & Central Asia',
                'North America', 'Northern & Western Europe'
            ),
            'overrepresented',
            'underrepresented'
        ) AS category_status,
        IF(
            c.wiki_db IN ('enwiki', 'ruwiki', 'dewiki', 'jawiki', 'frwiki', 'zhwiki', 'eswiki', 'itwiki'), -- tentative list of developing Wikis
            'developed',
            'emerging'
        ) AS wiki_status,
        c.metrics.standard_quality_count AS quality_article_count
    FROM
        content_gap_metrics.by_category c
    LEFT JOIN
        canonical_data.wikis w
    ON
        c.wiki_db = w.database_code
    WHERE
        c.time_bucket = '{metrics_month}'
        AND w.database_group = 'wikipedia'
        AND c.content_gap IN ('gender', 'geography_wmf_region')
)
SELECT
    month,
    content_gap,
    category_status,
    wiki_status,
    SUM(quality_article_count) AS quality_article_count
FROM
    quality_article_counts
GROUP BY
    month,
    content_gap,
    category_status,
    wiki_status
ORDER BY
    month,
    content_gap,
    category_status,
    wiki_status;