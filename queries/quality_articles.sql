SELECT
    mf.time_bucket as month,
    SUM(mf.standard_quality) AS `quality articles`,
    SUM(
        CASE
            WHEN mf.wiki_db IN ('enwiki', 'ruwiki', 'dewiki', 'jawiki', 'frwiki', 'zhwiki', 'eswiki', 'itwiki') THEN mf.standard_quality
            ELSE 0
        END
    ) AS `quality articles in developed Wikipedias`,
    SUM(
        CASE
            WHEN mf.wiki_db NOT IN ('enwiki', 'ruwiki', 'dewiki', 'jawiki', 'frwiki', 'zhwiki', 'eswiki', 'itwiki') THEN mf.standard_quality
            ELSE 0
        END
    ) AS `quality articles in emerging Wikipedias`
FROM
    content_gap_metrics.metric_features mf
LEFT JOIN
    canonical_data.wikis w
    ON mf.wiki_db = w.database_code
WHERE
    w.database_group = 'wikipedia'
    AND mf.time_bucket = '{metrics_month}'
GROUP BY
    mf.time_bucket
