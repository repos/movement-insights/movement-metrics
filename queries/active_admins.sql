WITH filtered_logging AS (
    SELECT
        pa.actor_name AS actor_name,
        CASE
            WHEN l.wiki_db IN (
                'enwiki', 'ruwiki', 'dewiki', 'jawiki', 
                'frwiki', 'zhwiki', 'eswiki', 'itwiki'
            ) THEN 'developed'
            ELSE 'emerging'
        END AS wiki_type
    FROM wmf_raw.mediawiki_logging l
    JOIN wmf_raw.mediawiki_private_actor pa
        ON l.log_actor = pa.actor_id 
        AND l.wiki_db = pa.wiki_db 
        AND pa.snapshot = '{mediawiki_history_snapshot}'
    JOIN canonical_data.wikis cw
        ON l.wiki_db = cw.database_code
        AND cw.database_group IN (
            "commons",
            "incubator",
            "foundation",
            "mediawiki",
            "meta",
            "sources",
            "species",
            "wikibooks",
            "wikidata",
            "wikifunctions",
            "wikinews",
            "wikipedia",
            "wikiquote",
            "wikisource",
            "wikiversity",
            "wikivoyage",
            "wiktionary"
        )
    WHERE
        l.log_type IN ('block', 'delete', 'protect', 'rights')
        AND l.log_action NOT IN ('autopromote', 'delete_redir', 'move_prot')
        AND SUBSTR(log_timestamp, 1, 6) = REPLACE('{metrics_month}', '-', '')
        AND l.snapshot = '{mediawiki_history_snapshot}'
),
counts AS (
    SELECT
        wiki_type,
        COUNT(DISTINCT actor_name) AS active_admins 
    FROM filtered_logging
    GROUP BY wiki_type WITH ROLLUP 
)
SELECT
    '{metrics_month_first_day}' AS month,
    SUM(IF(wiki_type IS NULL, active_admins, 0)) AS `active administrators`,
    SUM(IF(wiki_type = 'emerging', active_admins, 0)) AS `active administrators emerging wikis`,
    SUM(IF(wiki_type = 'developed', active_admins, 0)) AS `active administrators developed wikis`
FROM counts
GROUP BY '{metrics_month_first_day}';
