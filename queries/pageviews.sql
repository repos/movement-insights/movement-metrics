SELECT
    CONCAT(year, '-', month, '-01') AS month,
    SUM(desktop) AS `desktop pageviews`,
    SUM(mobileweb) AS `mobile web pageviews`,
    SUM(total) AS pageviews
FROM wmf_product.pageviews_corrected
WHERE
    year = {metrics_year}
    AND month = {metrics_cur_month}
GROUP BY CONCAT(year, '-', month, '-01')
