WITH user_registration AS (
    SELECT
        user_text AS username,
        event_user_creation_timestamp AS registration_date,
        TRUNC(event_user_creation_timestamp, "MONTH") AS month,
        CASE
            WHEN wiki_db IN (
                'enwiki', 'ruwiki', 'dewiki', 'jawiki',
                'frwiki', 'zhwiki', 'eswiki', 'itwiki'
            ) THEN 'developed'
            ELSE 'emerging'
        END AS wiki_type
    FROM wmf.mediawiki_history
    INNER JOIN canonical_data.wikis ON wiki_db = database_code
        AND database_group IN (
            "commons",
            "incubator",
            "foundation",
            "mediawiki",
            "meta",
            "sources",
            "species",
            "wikibooks",
            "wikidata",
            "wikifunctions",
            "wikinews",
            "wikipedia",
            "wikiquote",
            "wikisource",
            "wikiversity",
            "wikivoyage",
            "wiktionary"
        )
    WHERE event_entity = 'user'
        AND event_type = 'create'
        AND event_user_creation_timestamp IS NOT NULL -- Exclude rows with missing registration dates
        AND NOT user_is_created_by_system
        AND snapshot = '{mediawiki_history_snapshot}'
        AND SIZE(event_user_is_bot_by) = 0
        AND SIZE(event_user_is_bot_by_historical) = 0
)
SELECT
    month,
    COUNT(username) AS `account registrations`,
    SUM(CASE WHEN wiki_type = 'developed' THEN 1 ELSE 0 END) AS `account registrations developed wikis`,
    SUM(CASE WHEN wiki_type = 'emerging' THEN 1 ELSE 0 END) AS `account registrations emerging wikis`
FROM user_registration
WHERE month = '{metrics_month}'
GROUP BY month
ORDER BY month;