WITH global_registration_dates AS (
    SELECT
        e.wiki,
        e.month,
        e.user_name,
        e.user_id,
        e.content_edits,
        g.global_registration_month,
        CASE
            WHEN e.wiki IN (
                'enwiki', 'ruwiki', 'dewiki', 'jawiki', 
                'frwiki', 'zhwiki', 'eswiki', 'itwiki'
            ) THEN 'developed'
            ELSE 'emerging'
        END AS wiki_type
    FROM wmf_product.editor_month e
    INNER JOIN (
        SELECT
            user_name,
            MIN(CAST(TRUNC(user_registration, 'MONTH') AS DATE)) AS global_registration_month
        FROM wmf_product.editor_month
        WHERE user_name NOT LIKE '~2%' -- Filters out temporary accounts
        GROUP BY user_name
    ) g ON e.user_name = g.user_name
    WHERE
        e.user_id != 0
        AND NOT e.bot_by_group
        AND e.user_name NOT REGEXP 'bot\b'
        AND month = '{metrics_month_first_day}'
),
regional_totals AS (
    SELECT
        month,
        COUNT(DISTINCT CASE WHEN wiki_type = 'developed' THEN user_name END) AS active_editors_developed_wikis,
        COUNT(DISTINCT CASE WHEN wiki_type = 'emerging' THEN user_name END) AS active_editors_emerging_wikis,
        COUNT(DISTINCT CASE WHEN wiki_type = 'developed' AND global_registration_month = month THEN user_name END) AS new_active_editors_developed_wikis,
        COUNT(DISTINCT CASE WHEN wiki_type = 'emerging' AND global_registration_month = month THEN user_name END) AS new_active_editors_emerging_wikis
    FROM global_registration_dates
    WHERE content_edits >= 5
    GROUP BY month
),
global_editors AS (
    SELECT
        month,
        user_name,
        SUM(content_edits) AS total_content_edits,
        global_registration_month
    FROM global_registration_dates
    GROUP BY month, user_name, global_registration_month
),
global_totals AS (
    SELECT
        month,
        COUNT(DISTINCT user_name) AS `active editors`,
        COUNT(DISTINCT CASE WHEN global_registration_month = CAST(month AS DATE) THEN user_name END) AS `new active editors`,
        COUNT(DISTINCT user_name) - COUNT(DISTINCT CASE WHEN global_registration_month = CAST(month AS DATE) THEN user_name END) AS `returning active editors`
    FROM global_editors
    WHERE total_content_edits >= 5
    GROUP BY month
)
SELECT
    gt.month AS `month`,
    gt.`active editors`,
    gt.`new active editors`,
    gt.`returning active editors`,
    rt.active_editors_developed_wikis AS `active editors developed wikis`,
    rt.active_editors_emerging_wikis AS `active editors emerging wikis`,
    rt.new_active_editors_developed_wikis AS `new active editors developed wikis`,
    rt.new_active_editors_emerging_wikis AS `new active editors emerging wikis`
FROM global_totals gt
LEFT JOIN regional_totals rt
    ON gt.month = rt.month;
