WITH country AS (
    SELECT
        iso_code AS country_code,
        CASE 
            WHEN wikimedia_region IN ('Central & Eastern Europe & Central Asia',
                                      'North America', 
                                      'Northern & Western Europe')
            THEN 'represented'
            ELSE 'underrepresented'
        END AS category
    FROM canonical_data.countries
)
SELECT
    CONCAT(month, '-01') AS month,
    SUM(IF(country.category = 'represented', namespace_zero_distinct_editors, 0)) AS `active editors represented regions`,
    SUM(IF(country.category = 'underrepresented', namespace_zero_distinct_editors, 0)) AS `active editors underrepresented regions`
FROM
    wmf.geoeditors_monthly gm
JOIN
    country
    ON gm.country_code = country.country_code
WHERE
    NOT users_are_anonymous 
    AND activity_level != '1 to 4' 
    AND month = '{metrics_month}'
GROUP BY 
    month
ORDER BY 
    month;
    