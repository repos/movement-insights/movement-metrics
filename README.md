This repo contains the code needed to calculate the monthly and quarterly Wikimedia movement metrics.

For full instructions for the reporting process, see [mw:Movement Insights/Movement metrics process](https://meta.wikimedia.org/wiki/Movement_Insights/Movement_metrics_process#Run_the_notebooks).

## Setup
The calculation code is designed to run on one of the [analytics client servers](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Clients) servers and will not work elsewhere.

1. Clone this onto your chosen [analytics client server](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Clients) server.
2. Install the dependencies by running the following command in the project folder: `conda env update -f env.yaml`.

## Use
1. Run the notebook [monthly_report.ipynb](monthly_report.ipynb), which actually calculates the metrics and inserts them into several files in the `metrics` directory. The rest of the notebook does a few simple transformations on the metrics and produces the table of values needed for the final report, as well as a graph of each metric.
2. Commit and push the resulting changes to the GitHub repo with the commit message "Update MONTH YEAR metrics". You do not need to have the changes reviewed first. For information on how to connect to GitHub from one of the analytics clients, see [wikitech:Data Engineering/Systems/Jupyter#GitHub or GitLab](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Jupyter#GitHub_or_GitLab).
