from datetime import datetime
import sys

import matplotlib.pyplot as plt
import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart


def main():
    print("Formatting may look incorrect in jupyter notebook window but the saved chart image will still be correct.")

    #---PARAMETERS---
    save_file_name = "Unique_Devices.png"
    display_flag = True

    #---CLEAN DATA---
    #---CLEAN DATA---

    df = load_metric_file('unique_devices', month_as_period_index=False)

    start_date = "2018-05-01"
    end_date = pd.Timestamp.today()

    df = df[df["month"].isin(pd.date_range(start_date, end_date))]

    month_interest = df.iloc[-1]['month'].month

    # Drop rows with data error
    df_a = df[df["month"] <= "2021-01-01"]
    df_b = df[(df["month"] >= "2022-07-01") & (df["month"] < "2024-07-01")]
    df_c = df[df["month"] > "2024-07-31"]

    monthly_df_a = df_a[df_a['month'].dt.month == month_interest]
    monthly_df_b = df_b[df_b['month'].dt.month == month_interest]
    monthly_df_c = df_c[df_c['month'].dt.month == month_interest]
    monthly_df = pd.concat([monthly_df_a, monthly_df_b, monthly_df_c])

    # Subset to highlight the last two months
    yoy_highlight = pd.concat([monthly_df.iloc[-2, :], monthly_df.iloc[-1, :]], axis=1).T

    #---MAKE CHART---
    chart = Wikichart(start_date, end_date, df)
    chart.init_plot(width=12)

    # Plot data, because of the break in the dataset, we don't use the wikichart class functions

    plt.plot(df_a.month, df_a["unique devices"], 
             label='_nolegend_', 
             color=wmf_colors['brightblue'], 
             linewidth=2, zorder=6)
    plt.plot(df_b.month, df_b["unique devices"], 
             label='_nolegend_', 
             color=wmf_colors['brightblue'], 
             linewidth=2, zorder=6)
    plt.plot(df_c.month, df_c["unique devices"], 
             label='_nolegend_', 
             color=wmf_colors['brightblue'], 
             linewidth=2, zorder=6)

    plt.scatter(monthly_df.month, monthly_df["unique devices"], 
                label='_nolegend_', color=wmf_colors['brightblue'], zorder=7)

    highlight_radius = 1000000
    plt.scatter(yoy_highlight.month, 
                yoy_highlight["unique devices"], 
                label='_nolegend_', 
                s=(highlight_radius**0.5), 
                facecolors='none', 
                edgecolors=wmf_colors['yellow'], 
                linewidth=2,
                zorder=8)

    # Draw error area
    block_off_start = datetime.strptime("2021-01-01", '%Y-%m-%d')
    block_off_end = datetime.strptime("2022-07-01", '%Y-%m-%d')
    chart.block_off(block_off_start, 
                    block_off_end, 
                    rectangle_text="Data unreliable:\n• February 2021 to June 2022\n• July 2024", )

    
    block_off_start_new = datetime.strptime("2024-07-01", '%Y-%m-%d')
    block_off_end_new = datetime.strptime("2024-08-01", '%Y-%m-%d')
    chart.block_off(block_off_start_new, 
                    block_off_end_new, 
                    rectangle_text="")

    chart.format(title=f'Unique Devices - Wikipedia Only', 
                 radjust=0.8)

    chart.annotate(x='month', 
                   y='unique devices', 
                   num_annotation=chart.prepare_final_value_annotation(y='unique devices'))

    chart.finalize_plot(save_file_name, display=display_flag)


if __name__ == "__main__":
    main()





