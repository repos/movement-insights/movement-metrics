import pandas as pd
from datetime import datetime
from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart

def main():
    print("Generating Account Registrations Charts")
    
    #---PARAMETERS---
    save_file_name_total = "Account_Registrations_Total.png"
    save_file_name_area = "Account_Registrations_By_Wiki_Type.png"
    
    #-- CLEAN DATA--
    df = load_metric_file("contributors", month_as_period_index=False)
    
    # Date range of the charts will always start 5 years before the latest month available in our month column
    end_date = df['month'].max()
    start_date = (pd.to_datetime(end_date) - pd.DateOffset(years=5))
    
    df = df[df["month"].between(start_date, end_date)]
    
    key_colors = {
        'total': wmf_colors['purple'],
        'developed': wmf_colors['blue'],
        'emerging': wmf_colors['brightgreen']
    }
    
    # Total account registrations (Line Chart)
    chart_total = Wikichart(start_date, end_date, df)
    chart_total.init_plot(height=3, width=10)
    chart_total.plot_line('month', 'account registrations', key_colors['total'])
    chart_total.plot_monthlyscatter('month', 'account registrations', key_colors['total'])
    chart_total.plot_yoy_highlight(
        x='month', 
        y='account registrations', 
        highlight_radius=900 
    )
    chart_total.annotate(
        x='month',
        y='account registrations',
        num_annotation=chart_total.prepare_yoy_annotation(y='account registrations'),
        legend_label="Total Registrations",
        label_color=key_colors['total']
    )
    chart_total.format(title='Total Account Registrations', titlepad=15)
    chart_total.finalize_plot(save_file_name_total)

    # Area Chart for Developed and Emerging.
    
    df['stacked_developed'] = df['account registrations developed wikis'] + df['account registrations emerging wikis']

    chart_wiki_type = Wikichart(start_date, end_date, df)
    chart_wiki_type.init_plot(height=3, width=10)

    chart_wiki_type.plot_area(
        x='month', 
        y='stacked_developed', 
        col=key_colors['developed'], 
        alpha=0.8, 
        legend_label='Developed Wikis'
    )

    chart_wiki_type.plot_area(
        x='month', 
        y='account registrations emerging wikis', 
        col=key_colors['emerging'], 
        alpha=0.8, 
        legend_label='Emerging Wikis'
    )

    ys = ['stacked_developed', 'account registrations emerging wikis']
    calculation_ys = ['account registrations developed wikis', 'account registrations emerging wikis']


    key = pd.DataFrame({
        'labelname': ['Developed Wikis', 'Emerging Wikis'],
        'color': [key_colors['developed'], key_colors['emerging']]
    }, index=ys)

    chart_wiki_type.multi_annotate(
        ys=ys,
        key=key,
        annotation_fxn=chart_wiki_type.prepare_yoy_annotation,
        x='month',
        is_area_chart = True,
        calculation_ys = calculation_ys
    )

    chart_wiki_type.plot_monthlyscatter('month', 'account registrations emerging wikis', key_colors['emerging'])
    chart_wiki_type.plot_yoy_highlight(
        x='month', 
        y='account registrations emerging wikis', 
        highlight_radius=250
    )

    chart_wiki_type.plot_monthlyscatter('month', 'stacked_developed', key_colors['developed'])
    chart_wiki_type.plot_yoy_highlight(
        x='month', 
        y='stacked_developed', 
        highlight_radius=250
    )

    chart_wiki_type.format(title='Account Registrations Developed and Emerging Wikis', titlepad=15)
    chart_wiki_type.finalize_plot(save_file_name_area)

    if __name__ == "__main__":
        main()