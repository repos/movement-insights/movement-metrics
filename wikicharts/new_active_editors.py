from datetime import datetime
import pandas as pd
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart
from src.utils import load_metric_file

def main():
    print("Generating Charts: New Active Editors")
    
    save_file_name_new_editors_total = "New_Active_Editors_Total.png"
    save_file_name_new_editors_area = "New_Active_Editors_By_Wiki_Type.png"

    df = load_metric_file("contributors", month_as_period_index=False)
    
    # Date range of the charts will always start 5 years before the latest month available in our month column
    end_date = df['month'].max()
    start_date = (pd.to_datetime(end_date) - pd.DateOffset(years=5))
    
    df = df[df["month"].between(start_date, end_date)]

    key_colors = {
        'total': wmf_colors['purple'],
        'developed': wmf_colors['blue'],
        'emerging': wmf_colors['brightgreen']
    }
    
    # Total new active editors (Line Chart)
    chart_new_editors_total = Wikichart(start_date, end_date, df)
    chart_new_editors_total.init_plot(height=3, width=10)
    chart_new_editors_total.plot_line('month', 'new active editors', key_colors['total'])
    chart_new_editors_total.plot_monthlyscatter('month', 'new active editors', key_colors['total'])
    chart_new_editors_total.plot_yoy_highlight(
        x='month', 
        y='new active editors', 
        highlight_radius=900
    )
    chart_new_editors_total.annotate(
        x='month',
        y='new active editors',
        num_annotation=chart_new_editors_total.prepare_yoy_annotation(y='new active editors'),
        legend_label="Total New Active Editors",
        label_color=key_colors['total']
    )
    chart_new_editors_total.format(title='Total New Active Editors', titlepad=15)
    chart_new_editors_total.finalize_plot(save_file_name_new_editors_total)

    # New Active Editors - Developed and Emerging (Area Chart)
    chart_new_editors_area = Wikichart(start_date, end_date, df)
    chart_new_editors_area.init_plot(height=3, width=10)
    
    df['stacked_emerging'] = df['new active editors developed wikis'] + df['new active editors emerging wikis']

    
    chart_new_editors_area.plot_area(
        x='month', 
        y='stacked_emerging', 
        col=key_colors['emerging'], 
        alpha=0.8, 
        legend_label='Emerging Wikis'
    
    )

    chart_new_editors_area.plot_area(
        x='month', 
        y='new active editors developed wikis', 
        col=key_colors['developed'], 
        alpha=0.8, 
        legend_label='Developed Wikis'
    
    )
    

    # Define annotations for each series
    ys_new_editors = ['new active editors developed wikis', 'stacked_emerging']
    calculation_ys = ['new active editors developed wikis','new active editors emerging wikis']
    
    
    key_new_editors = pd.DataFrame({
        'labelname': ['Developed Wikis', 'Emerging Wikis'],
        'color': [key_colors['developed'], key_colors['emerging']]
    }, index=ys_new_editors)

   
    chart_new_editors_area.multi_annotate(
        ys=ys_new_editors,
        key=key_new_editors,
        annotation_fxn=chart_new_editors_area.prepare_yoy_annotation,
        x='month',
        is_area_chart = True,
        calculation_ys = calculation_ys
    )
    
    chart_new_editors_area.plot_monthlyscatter('month', 'new active editors developed wikis', key_colors['developed'])
    chart_new_editors_area.plot_yoy_highlight(
        x='month', 
        y='new active editors developed wikis', 
        highlight_radius=250,
       
    )
    
    chart_new_editors_area.plot_monthlyscatter('month', 'stacked_emerging', key_colors['emerging'])
    chart_new_editors_area.plot_yoy_highlight(
        x='month', 
        y='stacked_emerging', 
        highlight_radius=250
    )


    chart_new_editors_area.format(title='New Active Editors Developed and Emerging Wikis', titlepad=15)
    chart_new_editors_area.finalize_plot(save_file_name_new_editors_area)


if __name__ == "__main__":
    main()