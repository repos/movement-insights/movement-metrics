import sys

import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart


def main():
    #---PARAMETERS---
    returning_editors_filename = "Returning_Editors.png"
    display_flag = True

    #---CLEAN DATA---
    df = load_metric_file("contributors", month_as_period_index=False)
 
    start_date = "2019-01-01"
    end_date = pd.Timestamp.today()

    df = df[df["month"].isin(pd.date_range(start_date, end_date))]

    colors = {'Returning': wmf_colors['blue']}

    #---MAKE CHART FOR RETURNING EDITORS---
    chart = Wikichart(start_date, end_date, df)
    chart.init_plot(width=12)
    chart.plot_line('month', 'returning active editors', colors['Returning'])
    chart.plot_monthlyscatter('month', 'returning active editors', colors['Returning'])
    chart.plot_yoy_highlight('month', 'returning active editors')
    chart.format(title='Returning Active Editors',
                 radjust=0.75,
                 )
    
    chart.plot_yoy_highlight('month','returning active editors')
    chart.annotate(x='month',
        y='returning active editors',
        num_annotation=chart.prepare_yoy_annotation(y='returning active editors'))

    
    chart.finalize_plot(returning_editors_filename, display=display_flag)

    

if __name__ == "__main__":
    main()





