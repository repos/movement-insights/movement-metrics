import sys

import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart


def main():
    #---PARAMETERS---
    save_file_name = "Content_Interactions.png"
    display_flag = True

    start_date = "2018-05-01"
    end_date = pd.Timestamp.today()

    #---CLEAN DATA--
    # Wrangle data so that we have a frame with three columns:
    # - month
    # - interactions (raw values)
    # - interactions_corrected (corrected for data loss)

    df = (
        load_metric_file("content_interactions", month_as_period_index=False)
        # Setting the "month" as an index temporarily allows us to combine
        # series very efficiently
        .set_index("month")
        ["content interactions"]
        .rename("interactions")
        # This is the base for our final dataframe, so we don't want to leave it
        # as a series
        .to_frame()
    )

    interactions_corrected = (
       load_metric_file("corrected_metrics_only", month_as_period_index=False)
        .set_index("month")
        ["interactions_corrected"]
    )

    # Produce a full interactions_corrected column by starting with the corrected values and
    # then filling the remaining months using the raw values
    df["interactions_corrected"] = interactions_corrected.combine_first(df["interactions"])

    # The charting code expects "month" to be a regular column, not an index
    df = df.reset_index()

    #---MAKE CHART---

    chart = Wikichart(start_date,end_date,df)
    chart.init_plot()
   
    chart.plot_data_loss('month','interactions','interactions_corrected',df)
    
    chart.plot_line('month','interactions_corrected',wmf_colors['blue'])
    
    chart.plot_monthlyscatter('month','interactions_corrected',wmf_colors['blue'])
    chart.plot_yoy_highlight('month','interactions_corrected')
    
    chart.format(title = f'Content Interactions',
        radjust=0.87)
    
    chart.annotate(x='month',
        y='interactions_corrected',
        num_annotation=chart.prepare_yoy_annotation(y='interactions_corrected'))
    
    chart.finalize_plot(save_file_name,display=display_flag)
    
    
    

if __name__ == "__main__":
    main()
