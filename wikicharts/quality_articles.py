import sys
from datetime import datetime

import matplotlib.pyplot as plt
import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart


def main():
    print("Content Gaps Chart: quality articles")
    
    #---PARAMETERS---
    save_file_name_line = "Quality_articles_line.png"
    save_file_name_bar = "Quality_articles_bar.png"
    
    #-- CLEAN DATA--
    df = load_metric_file("content_gaps", month_as_period_index=False)

   # Date range of the charts will always start 5 years before the latest month available in our month column
    end_date = df['month'].max().replace(day=1).strftime('%Y-%m-%d')
    start_date = (pd.to_datetime(end_date) - pd.DateOffset(years=5)).replace(day=1).strftime('%Y-%m-%d')

    df = df[df["month"].isin(pd.date_range(start_date, end_date))]
    
    
    #---MAKE LINE CHART---
    
    key = pd.DataFrame([['All Wikipedias',wmf_colors['purple']],
        ['Developed',wmf_colors['pink']],
        ['Emerging',wmf_colors['yellow']]],
        index=['quality articles',
               'quality articles in developed Wikipedias', 
               'quality articles in emerging Wikipedias'],
        columns=['labelname','color'])
        
        
    chart = Wikichart(start_date, end_date, df)
    chart.init_plot(height = 3, width = 10)
    
    
    chart.plot_line('month', 
                         'quality articles', 
                         key.loc['quality articles','color'])
    
    chart.plot_line('month', 
                         'quality articles in developed Wikipedias', 
                         key.loc['quality articles in developed Wikipedias', 'color'])
    
    chart.plot_line('month', 
                         'quality articles in emerging Wikipedias', 
                         key.loc['quality articles in emerging Wikipedias', 'color'])
    
    
    chart.format(title='Total Quality Articles',
         titlepad=15,
         perc = False,
         ybuffer = False)


    chart.multi_annotate(['quality articles',
                              'quality articles in developed Wikipedias',
                              'quality articles in emerging Wikipedias'],
                              key,chart.prepare_yoy_annotation, 
                              xpad=5)
    
  
    ax_bar = plt.gca()
    ax_bar.set_yticklabels([f'{int(y / 1e6)}M' for y in ax_bar.get_yticks()])
    
    for label in key.index:
        # Annotate last y value
        last_x = df['month'].iloc[-1]
        last_y = df[label].iloc[-1]
        plt.annotate(f'{last_y / 1e6:.1f}M',
                     xy=(last_x, last_y), 
                     xytext=(0, 11),  
                     textcoords='offset points',
                     ha='center',
                     color=key.loc[label, 'color'],
                     fontsize=10, 
                     weight='bold',
                     bbox=dict(pad=5, facecolor="white", edgecolor="none", alpha=0.7))
        

    
    chart.finalize_plot(save_file_name_line)
    
 #---MAKE BAR CHART---
    chart_bar = Wikichart(start_date, end_date, df, time_col='month')
    chart_bar.init_plot(height=2)
    
    bars = chart_bar.plot_bar('month', 'new quality articles', wmf_colors['purple'], width=14)

   
    # Annotate last bar
    last_x = df['month'].iloc[-1]
    last_y = df['new quality articles'].iloc[-1]
    plt.annotate(f'{last_y / 1e3:.0f}K',
                 xy=(last_x, last_y),
                 xytext=(2, 3), 
                 textcoords='offset points',
                 ha='center', va='bottom',
                 fontsize=10, weight='bold',
                 )
    
    chart_bar.format(
        title='',
        titlepad=15,
        perc=False,
        badjust=0.25
    )
 
    key = pd.DataFrame([
    ['new quality articles', wmf_colors['purple']],
    ],
    index=[
        'new quality articles',
    ],
    columns=['labelname', 'color']
    )
    
    chart.multi_annotate(['new quality articles'
                             ],
                              key,chart.prepare_mom_annotation, 
                              xpad=10)
    
    
    chart_bar.finalize_plot(save_file_name_bar)
    
if __name__ == "__main__":
    main()