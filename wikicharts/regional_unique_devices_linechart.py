from datetime import datetime
from math import ceil
import os
import sys
import warnings

import matplotlib.pyplot as plt
import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import key_colors, wmf_regions
from wikicharts.data_utils import gen_keys
from wikicharts.wikicharts import Wikichart


def main():
    print("Formatting may look incorrect in jupyter notebook window but the saved chart image will still be correct.")

    warnings.filterwarnings("ignore")
    
    save_file_name_base = "Regional_Unique_Devices"

    #---CLEAN DATA---

    df = load_metric_file('unique_devices', month_as_period_index=False)

    df = df.drop('unique devices', axis=1)

    # Date range of the charts will always start 5 years before the latest month available in our month column
    end_date = df['month'].max()
    start_date = (pd.to_datetime(end_date) - pd.DateOffset(years=5))
    
    df = df[df["month"].between(start_date, end_date)]

    block_off_start = datetime.strptime("2021-01-01", '%Y-%m-%d')
    block_off_end = datetime.strptime("2022-07-01", '%Y-%m-%d')

    # New block for July 2024
    block_off_single_month = datetime.strptime("2024-07-01", '%Y-%m-%d')

    df = df[df["month"].isin(pd.date_range(start_date, end_date))]

    # Exclude rows for the new gap (July 2024)
    df = df[(df["month"] != block_off_single_month)]

    # Rename columns to nicely-formatted region names
    column_names_to_region_names = {
        'Central & Eastern Europe & Central Asia unique devices': 'Central & Eastern Europe & Central Asia',
        'East, Southeast Asia, & Pacific unique devices': 'East, Southeast Asia, & Pacific',
        'Latin America & Caribbean unique devices': 'Latin America & Caribbean',
        'Middle East & North Africa unique devices': 'Middle East & North Africa',
        'North America unique devices': 'North America',
        'Northern & Western Europe unique devices': 'Northern & Western Europe',
        'South Asia unique devices': 'South Asia',
        'Sub-Saharan Africa unique devices': 'Sub-Saharan Africa',
    }

    df = df.rename(column_names_to_region_names, axis='columns')
    df = df.set_index('month')

    columns_in_order = df.sum().sort_values(ascending=False).index

    df = df[columns_in_order]

    # gen_keys expects a list of data frames, with no index
    dfs = [df[wmf_regions].reset_index(), df[df.columns[~df.columns.isin(wmf_regions)]].reset_index()]
    df = df.reset_index()

    #---MAKE CHART---
    max_charts_per_figure = 8
    keys = gen_keys(dfs, key_colors)

    annotation_text = "     Data unreliable [February 2021 - June 2022, July 2024] (periods not shown)"
    fig_counter = 0
    total_num_charts = len(df.columns) - 1
    num_figures = ceil(total_num_charts / max_charts_per_figure)
    figures = [None] * num_figures
    maxranges = [None] * num_figures
    num_ticks = [None] * num_figures

    # Initialize each figure
    for f in range(num_figures):
        charts_in_figure = len(dfs[f].columns) - 1
        figures[f] = Wikichart(start_date, end_date, dfs[f])
        figures[f].init_plot(width=12, subplotsx=2, subplotsy=4, fignum=f)
        figures[f].plot_subplots_lines('month', keys[f], num_charts=charts_in_figure, subplot_title_size=9)
        figures[f].plot_multi_trendlines('month', keys[f], num_charts=charts_in_figure)
        maxranges[f], num_ticks[f] = figures[f].get_maxyrange()

    # Calculate the largest range between the two figures and multiple subplots
    maxrange = max(maxranges)
    maxrange_index = maxranges.index(maxrange)
    maxrange_numticks = num_ticks[maxrange_index]

    # Plot regional linechart and save file
    for f in range(num_figures):
        plt.figure(f)
        charts_in_figure = len(dfs[f].columns) - 1
        figures[f].standardize_subplotyrange(maxrange, maxrange_numticks, num_charts=charts_in_figure)
        figures[f].block_off_multi(block_off_start, block_off_end)
        figures[f].block_off_multi(block_off_single_month, block_off_single_month)
        figures[f].format_subplots(title='Regional Unique Devices',
                                   key=keys[f],
                                   tadjust=0.8, badjust=0.1,
                                   num_charts=charts_in_figure,
                                   tickfontsize=8)
        figures[f].clean_ylabels_subplots(tickfontsize=8)
        figures[f].top_annotation(annotation_text=annotation_text)
        save_file_name = save_file_name_base + "_All" + ".png"
        figures[f].finalize_plot(save_file_name, display=False)
        fig_counter += 1

    #---INDIVIDUAL CHARTS---
    individual_charts = [None] * total_num_charts
    columns = list(df.columns)
    columns.remove('month')

    for c in range(len(columns)):
        current_col = columns[c]
        current_df = df[['month', current_col]]
        current_savefile = save_file_name_base + "_" + f'{current_col}' + ".png"
        individual_charts[c] = Wikichart(start_date, end_date, current_df)
        individual_charts[c].init_plot(fignum=fig_counter)

        current_color = key_colors[(c % len(key_colors))]
        individual_charts[c].plot_line('month', current_col, current_color)
        individual_charts[c].plot_monthlyscatter('month', current_col, col=current_color)
        individual_charts[c].plot_yoy_highlight('month', current_col)
        current_yrange = individual_charts[c].get_ytickrange()

        if current_yrange > (maxrange / 8):
            individual_charts[c].standardize_yrange(maxrange, maxrange_numticks)

        individual_charts[c].block_off(block_off_start, block_off_end,
                                       rectangle_text="Data unreliable:\n• February 2021 to June 2022\n• July 2024")
        individual_charts[c].block_off(block_off_single_month, block_off_single_month,
                                       rectangle_text="")

        individual_charts[c].format(title=f'Unique Devices: {current_col}',
                                    ybuffer=False,
                                    tadjust=0.825, badjust=0.125,
                                    titlepad=25)

        individual_charts[c].annotate(
            x='month',
            y=current_col,
            num_annotation=individual_charts[c].prepare_yoy_annotation(y=current_col)
        )

        individual_charts[c].finalize_plot(current_savefile, display=False)
        fig_counter += 1


if __name__ == "__main__":
    main()





