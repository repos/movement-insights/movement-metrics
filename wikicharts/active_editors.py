from datetime import datetime
import pandas as pd
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart
from src.utils import load_metric_file

def main():
    print("Generating Charts: Active Editors")
    
    save_file_name_editors_total = "Active_Editors_Total.png"
    save_file_name_editors_area = "Active_Editors_By_Wiki_Type.png"

    # Load and clean data
    df = load_metric_file("contributors", month_as_period_index=False)
    
   # Date range of the charts will always start 5 years before the latest month available in our month column
    end_date = df['month'].max()
    start_date = (pd.to_datetime(end_date) - pd.DateOffset(years=5))
    
    df = df[df["month"].between(start_date, end_date)]

    key_colors = {
        'total': wmf_colors['purple'],
        'developed': wmf_colors['blue'],
        'emerging': wmf_colors['brightgreen']
    }
    
    # Total active editors (Line Chart)
    chart_editors_total = Wikichart(start_date, end_date, df)
    chart_editors_total.init_plot(height=3, width=10)
    chart_editors_total.plot_line('month', 'active editors', key_colors['total'])
    chart_editors_total.plot_monthlyscatter('month', 'active editors', key_colors['total'])
    chart_editors_total.plot_yoy_highlight(
        x='month', 
        y='active editors', 
        highlight_radius=900, 
        
    )
    chart_editors_total.annotate(
        x='month',
        y='active editors',
        num_annotation=chart_editors_total.prepare_yoy_annotation(y='active editors'),
        legend_label="Total Editors",
        label_color=key_colors['total']
    )
    chart_editors_total.format(title='Total Active Editors', titlepad=15)
    chart_editors_total.finalize_plot(save_file_name_editors_total)

    # Active Editors - Developed and Emerging (Area Chart)
    chart_editors_area = Wikichart(start_date, end_date, df)
    chart_editors_area.init_plot(height=3, width=10)
    
    df['stacked developed wikis'] = df['active editors developed wikis'] + df['active editors emerging wikis']
    
    chart_editors_area.plot_area(
        x='month', 
        y='stacked developed wikis', 
        col=key_colors['developed'], 
        alpha=0.8, 
        legend_label='Developed Wikis'
    )
    chart_editors_area.plot_area(
        x='month', 
        y='active editors emerging wikis', 
        col=key_colors['emerging'], 
        alpha=0.8, 
        legend_label='Emerging Wikis'
    )

    ys_editors = ['stacked developed wikis', 'active editors emerging wikis']
    calculation_ys = ['active editors developed wikis', 'active editors emerging wikis']
    
    key_editors = pd.DataFrame({
        'labelname': ['Developed Wikis', 'Emerging Wikis'],
        'color': [key_colors['developed'], key_colors['emerging']]
    }, index=ys_editors)

    chart_editors_area.multi_annotate(
        ys=ys_editors,
        key=key_editors,
        annotation_fxn=chart_editors_area.prepare_yoy_annotation,
        x='month',
        is_area_chart = True,
        calculation_ys = calculation_ys
    )
    
    chart_editors_area.plot_monthlyscatter('month', 'active editors emerging wikis', key_colors['emerging'])
    chart_editors_area.plot_yoy_highlight(
        x='month', 
        y='active editors emerging wikis', 
        highlight_radius=250
    )
    
    chart_editors_area.plot_monthlyscatter('month', 'stacked developed wikis', key_colors['developed'])
    chart_editors_area.plot_yoy_highlight(
        x='month', 
        y='stacked developed wikis', 
        highlight_radius=250
    )


    chart_editors_area.format(title='Active Editors Developed and Emerging Wikis', titlepad=15)
    chart_editors_area.finalize_plot(save_file_name_editors_area)


if __name__ == "__main__":
    main()
