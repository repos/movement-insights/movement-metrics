import sys

import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart


def main():
    #---PARAMETERS---
    save_file_name = "Net_New_Content.png"
    display_flag = True

    #---CLEAN DATA--
    df = load_metric_file('content', month_as_period_index=False)
        
    start_date = "2018-05-01"
    end_date = pd.Timestamp.today()
    df = df[df["month"].isin(pd.date_range(start_date, end_date))]

    #---PREPARE TO PLOT
    key = pd.DataFrame([['Commons',wmf_colors['pink']],
        ['Wikidata',wmf_colors['brightgreen']],
        ['Wikipedia',wmf_colors['purple']]],
        index=['new Commons content pages','new Wikidata entities','new Wikipedia articles'],
        columns=['labelname','color'])

    #---MAKE CHART---
    chart = Wikichart(start_date,end_date,df)
    chart.init_plot(width=12)
    chart.plot_line('month',
                    'new Commons content pages',
                    key.loc['new Commons content pages',
                            'color'])
    
    chart.plot_line('month',
                    'new Wikidata entities',
                    key.loc['new Wikidata entities',
                            'color'])
    
    chart.plot_line('month','new Wikipedia articles',
                    key.loc['new Wikipedia articles',
                            'color'])

    chart.plot_monthlyscatter('month',
                              'new Commons content pages',
                              key.loc['new Commons content pages',
                                      'color'])
    
    chart.plot_monthlyscatter('month','new Wikidata entities',
                              key.loc['new Wikidata entities',
                                      'color'])
    
    chart.plot_monthlyscatter('month',
                              'new Wikipedia articles',
                              key.loc['new Wikipedia articles','color'])

    chart.format(title = f'Net New Content',
        radjust=0.75
    )

    chart.multi_annotate(['new Commons content pages',
                              'new Wikidata entities',
                              'new Wikipedia articles'],
                              key,chart.prepare_yoy_annotation, 
                              xpad=0)

    chart.finalize_plot(save_file_name,display=display_flag)

if __name__ == "__main__":
    main()



