import sys
from datetime import datetime

import matplotlib.pyplot as plt
import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart

def main():
    print("Content Gaps Chart")
    
    #---PARAMETERS---
    save_file_name_line = "Quality_bios_women_genderdiverse_line.png"
    save_file_name_bar = "Quality_bios_women_genderdiverse_bar.png"
    
    #-- CLEAN DATA--
    df = load_metric_file("content_gaps", month_as_period_index=False)
    
    # Date range of the charts will always start 5 years before the latest month available in our month column
    end_date = df['month'].max().replace(day=1).strftime('%Y-%m-%d')
    start_date = (pd.to_datetime(end_date) - pd.DateOffset(years=5)).replace(day=1).strftime('%Y-%m-%d')


    df = df[df["month"].isin(pd.date_range(start_date, end_date))]
    
    df['quality biographies about women and gender-diverse people'] = (
        df['quality biographies in developed Wikipedias of women and gender-diverse people'] + 
        df['quality biographies in emerging Wikipedias of women and gender-diverse people']
    )
    
    df['new quality biographies about women and gender-diverse people'] = ( 
        df['new quality biographies in developed Wikipedias of women and gender-diverse people'] + 
        df['new quality biographies in emerging Wikipedias of women and gender-diverse people']
    )
    
    
    #---MAKE LINE CHART---
    
    key = pd.DataFrame([
    ['All Wikipedias', wmf_colors['green']],
    ['Developed', wmf_colors['blue']],
    ['Emerging', wmf_colors['yellow']]
    ],
    index=[
        'quality biographies about women and gender-diverse people',
        'quality biographies in developed Wikipedias of women and gender-diverse people', 
        'quality biographies in emerging Wikipedias of women and gender-diverse people'
    ],
    columns=['labelname', 'color']
    )


         
        
    chart = Wikichart(start_date, end_date, df)
    chart.init_plot(height = 3, width = 10)
    
    chart.plot_line('month', 
                         'quality biographies about women and gender-diverse people', 
                         key.loc['quality biographies about women and gender-diverse people', 'color'])
    
    chart.plot_line('month', 
                         'quality biographies in developed Wikipedias of women and gender-diverse people', 
                         key.loc['quality biographies in developed Wikipedias of women and gender-diverse people', 'color'])
    
    chart.plot_line('month', 
                         'quality biographies in emerging Wikipedias of women and gender-diverse people', 
                         key.loc['quality biographies in emerging Wikipedias of women and gender-diverse people', 'color'])
        
    
    chart.format(title='Quality Biographies of Women and Gender-diverse people',
         titlepad=15,
         perc = False)


    chart.multi_annotate(['quality biographies about women and gender-diverse people',
                             'quality biographies in developed Wikipedias of women and gender-diverse people',
                             'quality biographies in emerging Wikipedias of women and gender-diverse people',],
                              key,chart.prepare_yoy_annotation, 
                              xpad=5)
    
    for label in key.index:
        # Annotate last y value
        last_x = df['month'].iloc[-1]
        last_y = df[label].iloc[-1]
        plt.annotate(f'{last_y / 1e3:.0f}K',
                     xy=(last_x, last_y), 
                     xytext=(1, 11),  
                     textcoords='offset points',
                     ha='center',
                     color=key.loc[label, 'color'],
                     fontsize=10, 
                     weight='bold',
                     bbox=dict(pad=6, facecolor="white", edgecolor="none", alpha=0.7))

    chart.finalize_plot(save_file_name_line)
    
    
    #---MAKE BAR CHART---
    chart_bar = Wikichart(start_date, end_date, df, time_col='month')
    chart_bar.init_plot(height = 2)
    
    chart_bar.plot_bar('month', 'new quality biographies about women and gender-diverse people', 
                    wmf_colors['green'], width = 14)
    
    
     # Annotate last bar
    last_x = df['month'].iloc[-1]
    last_y = df['new quality biographies about women and gender-diverse people'].iloc[-1]
    plt.annotate(f'{last_y / 1e3:.0f}K',
                 xy=(last_x, last_y),
                 xytext=(0,3), 
                 textcoords='offset points',
                 ha='center', va='bottom',
                 fontsize=10, weight='bold',
                 )
    

    chart_bar.format(title='',
             titlepad=15,
             perc = False,
             badjust = 0.25)
    

    
    key = pd.DataFrame([
    ['new quality biographies', wmf_colors['green']],
    ],
    index=[
        'new quality biographies about women and gender-diverse people',
    ],
    columns=['labelname', 'color']
    )

    
    chart.multi_annotate(['new quality biographies about women and gender-diverse people'
                             ],
                              key,chart.prepare_mom_annotation, 
                              xpad=5)
    
    
    chart_bar.finalize_plot(save_file_name_bar)
    

if __name__ == "__main__":
    main()