from datetime import datetime
import pandas as pd
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart
from src.utils import load_metric_file

def main():
    print("Generating Charts: Active Administrators")
    
    save_file_name_administrators_total = "Active_Administrators_Total.png"
    save_file_name_administrators_area = "Active_Administrators_By_Wiki_Type.png"

    df = load_metric_file("contributors", month_as_period_index=False)
    
   # Date range of the charts will always start 5 years before the latest month available in our month column
    end_date = df['month'].max()
    start_date = (pd.to_datetime(end_date) - pd.DateOffset(years=5))
    
    df = df[df["month"].between(start_date, end_date)]

    key_colors = {
        'total': wmf_colors['purple'],
        'developed': wmf_colors['blue'],
        'emerging': wmf_colors['brightgreen']
    }
    
    # Total active administrators (Line Chart)
    chart_administrators_total = Wikichart(start_date, end_date, df)
    chart_administrators_total.init_plot(height=3, width=10)
    chart_administrators_total.plot_line('month', 'active administrators', key_colors['total'])
    chart_administrators_total.plot_monthlyscatter('month', 'active administrators', key_colors['total'])
    chart_administrators_total.plot_yoy_highlight(
        x='month', 
        y='active administrators', 
        highlight_radius=900
    )
    chart_administrators_total.annotate(
        x='month',
        y='active administrators',
        num_annotation=chart_administrators_total.prepare_yoy_annotation(y='active administrators'),
        legend_label="Total Administrators",
        label_color=key_colors['total']
    )
    chart_administrators_total.format(title='Total Active Administrators', titlepad=15)
    chart_administrators_total.finalize_plot(save_file_name_administrators_total)
    
    # Active administrators area chart

    df['stacked_emerging'] = df['active administrators developed wikis'] + df['active administrators emerging wikis']

    chart_wiki_type = Wikichart(start_date, end_date, df)
    chart_wiki_type.init_plot(height=3, width=10)

    chart_wiki_type.plot_area(
        x='month', 
        y='active administrators developed wikis', 
        col=key_colors['developed'],  
        alpha=0.8, 
        legend_label='Developed Wikis',
        zorder=3
    )

    chart_wiki_type.plot_area(
        x='month', 
        y='stacked_emerging', 
        col=key_colors['emerging'],  
        alpha=0.8, 
        legend_label='Emerging Wikis',
        zorder=2
    )

    ys_admin = ['active administrators developed wikis', 'stacked_emerging']
    calculation_ys = ['active administrators developed wikis', 'active administrators emerging wikis']
    
    key_admin = pd.DataFrame({
        'labelname': ['Developed Wikis', 'Emerging Wikis'],
        'color': [key_colors['developed'], key_colors['emerging']]
    }, index=ys_admin)

    chart_wiki_type.multi_annotate(
        ys=ys_admin,
        key=key_admin,
        annotation_fxn=chart_wiki_type.prepare_yoy_annotation,
        x='month',
        is_area_chart = True,
        calculation_ys = calculation_ys
    )

    chart_wiki_type.plot_monthlyscatter('month', 'active administrators developed wikis', key_colors['developed'])
    chart_wiki_type.plot_yoy_highlight(
        x='month', 
        y='active administrators developed wikis', 
        highlight_radius=250
    )

    chart_wiki_type.plot_monthlyscatter('month', 'stacked_emerging', key_colors['emerging'])
    chart_wiki_type.plot_yoy_highlight(
        x='month', 
        y='stacked_emerging', 
        highlight_radius=250
    )

    chart_wiki_type.format(title='Active Administrators Developed and Emerging Wikis', titlepad=15)
    chart_wiki_type.finalize_plot(save_file_name_administrators_area)


if __name__ == "__main__":
    main()