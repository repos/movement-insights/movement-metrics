import sys

import matplotlib.pyplot as plt
import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart


def main():
    #---PARAMETERS---
    save_file_name = "Pageviews_Referral.png"
    display_flag = True

    #---CLEAN DATA--
    df = (
        load_metric_file('referral_source', month_as_period_index=False)
        .rename(columns={
            'corrected_internal_view_count': 'internal',
            'corrected_none_view_count': 'none',
            'corrected_search_engine_view_count': 'external'
        })
        ['month', 'internal', 'none', 'external']
    )

    start_date = "2021-07-01"
    end_date = pd.Timestamp.today()

 
    df = df[df["month"].isin(pd.date_range(start_date, end_date))]

    #---PREPARE TO PLOT
    key = pd.DataFrame([['External (search engine)',wmf_colors['purple']],
        ['Internal',wmf_colors['green']],
        ['None',wmf_colors['orange']]],
        index=['external','internal','none'],
        columns=['labelname','color'])
 
    #---MAKE CHART---
    chart = Wikichart(start_date,
                      end_date,
                      df,
                      time_col='month')
    
    chart.init_plot(width=15,
                    height=7)
    
    plt.plot(df.month, 
             df.external, 
             label='_nolegend_', 
             color=key.loc['external','color'],
             linewidth=2,
             zorder=8)
    
    plt.plot(df.month, 
             df.internal, 
             label='_nolegend_', 
             color=key.loc['internal','color'],
             linewidth=2,
             zorder=8)
    
    plt.plot(df.month, 
             df.none, 
             label='_nolegend_', 
             color=key.loc['none','color'],
             linewidth=2,
             zorder=8)
   
    chart.format(title = 'Pageviews by Referral Source',
        ybuffer=True,
        format_x_yearly=True,
        badjust=0.275, ladjust=0.1, radjust=0.85,
        titlepad=20)
    
    plt.xlabel("Year",
               font='Montserrat', 
               fontsize=18, 
               labelpad=10) 
    
    plt.ylabel("Pageviews",
               font='Montserrat', 
               fontsize=18,
               labelpad=10)
    
    chart.multi_annotate(['external','internal','none'],key,chart.prepare_yoy_annotation,xpad=0)
    
    chart.plot_monthlyscatter('month','external',
                              key.loc['external','color'])
    
    chart.plot_monthlyscatter('month','internal',
                              key.loc['internal','color'])
    
    chart.plot_monthlyscatter('month','none',
                              key.loc['none','color'])
    
    plt.figtext(0.5, 0.09, "Data between July 2021 and January 2022 corrected for data loss.", 
                fontsize=14, 
                family='Montserrat',
                color= wmf_colors['black25'],
                horizontalalignment='center')
    
    chart.finalize_plot(save_file_name,display=display_flag)

if __name__ == "__main__":
    main()
