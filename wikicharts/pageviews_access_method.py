import sys

import matplotlib.pyplot as plt
import pandas as pd

sys.path.insert(0, "..")

from src.utils import load_metric_file
from wikicharts.config import wmf_colors
from wikicharts.wikicharts import Wikichart


def main():
    print("Note that even if annotation looks incorrect or cut off in jupyter notebook, the image saved in the charts folder may still be correct")

    #---PARAMETERS---
    save_file_name = "Pageviews_Access_Method.png"

    #---CLEAN DATA--
    df = load_metric_file('content_interactions', month_as_period_index=False)
    
    df = df[['month','desktop pageviews', 'mobile web pageviews']]
    
    start_date = "2022-02-01"
    end_date = pd.Timestamp.today()
    
    df = df[df["month"].isin(pd.date_range(start_date, end_date))]
    

    #---PLOT---
    
    chart = Wikichart(start_date,end_date,df,time_col='month')
    chart.init_plot()
    
    plt.plot(df["month"],
             df["desktop pageviews"],label='_nolegend_',
             color=wmf_colors['brightgreen'])
    
    plt.plot(df["month"], 
             df["mobile web pageviews"],
             label='_nolegend_',
             color=wmf_colors['pink'])
    
    plt.scatter(df["month"], 
                df["desktop pageviews"],
                label='_nolegend_',
                color=wmf_colors['brightgreen'])
    
    plt.scatter(df["month"] , 
                df["mobile web pageviews"],
                label='_nolegend_',
                color=wmf_colors['pink'])
    
    chart.format(title = f'Pageviews by Access Method',
        radjust=0.85,
        badjust=0.2,
        ladjust=0.1)
    
    plt.xlabel("",font='Montserrat', fontsize=18, labelpad=10)
    plt.ylabel("Pageviews",font='Montserrat', fontsize=18,labelpad=10)
    
    chart.annotate(x='month',
        y='desktop pageviews',
        legend_label="Desktop",
        num_color=wmf_colors['brightgreen'],
        num_annotation=chart.prepare_final_value_annotation(y='desktop pageviews'))
    
    chart.annotate(x='month',
        y='mobile web pageviews',
        legend_label="Mobile",
        num_color=wmf_colors['pink'],
        num_annotation=chart.prepare_final_value_annotation(y='mobile web pageviews'))
    
    chart.finalize_plot(save_file_name,
                        display=True)

if __name__ == "__main__":
    main()
