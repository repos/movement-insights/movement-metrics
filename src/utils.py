from numbers import Number
from pathlib import Path

import numpy as np
import pandas as pd

from wmfdata.utils import pct_str, sig_figs

# The absolute path to the directory where metric files can be found.
# All metric files are assumed to be in the top level of this directory.
project_dir = Path(__file__).resolve().parents[1]
metrics_dir = project_dir / "metrics"
wikicharts_data_dir = project_dir / "wikicharts" / "resources" / "data"

def find_metric_file(file_name):
    """
    Given the name of a metric file, with or without the ".tsv",
    returns the absolute path to the file as a pathlib.Path.
    """

    if not file_name.endswith(".tsv"):
        file_name = file_name + ".tsv"

    path_in_metrics_dir = metrics_dir / file_name
    path_in_wikicharts_data_dir = wikicharts_data_dir / file_name
    if path_in_metrics_dir.exists():
        return path_in_metrics_dir
    elif path_in_wikicharts_data_dir.exists():
        return path_in_wikicharts_data_dir
    else:
        raise OSError(f"{file_name} was not found in `metrics` or `wikicharts/resources/data`.")

def load_metric_file(file_name, month_as_period_index=True):
    """
    Given the name of a metric file, with or without the ".tsv", 
    returns the contents of the file as a Pandas data frame.

    The `month_as_period_index` argument is important because the calculation
    and reporting code expects the month as a Period index, while the 
    charting code expects the month as a datetime column.

    It's assumed that the files are in either `metrics` or  `wikicharts/resources/data`
    and that there are no name conflicts between the two locations. We should
    be able to stop searching in the second location after we complete T368995.
    """ 

    file_path = find_metric_file(file_name)

    df =  pd.read_csv(file_path, sep="\t", parse_dates=["month"])

    if month_as_period_index:
        df = df.set_index("month").to_period()

    return df

def load_all_metric_files(month_as_period_index=True):
    dfs = []

    for file in metrics_dir.iterdir():
        if file.suffix == ".tsv":
            # We always get the data frame with month_as_period_index,
            # and put it in the requested format later
            df = load_metric_file(file.name, month_as_period_index=True)
            dfs.append(df)
    
        metrics = dfs[0].join(dfs[1:], how="outer", sort=True)

    if not month_as_period_index:
        metrics = metrics.reset_index()
        metrics["month"] = metrics["month"].astype("datetime64")

    return metrics
    
def subtract_year(period):
    # As of Oct 2023, Pandas doesn't have a way to subtract
    # a year from an arbitrary period (we want to support both months
    # and quarters), so we have to cast to a timestamp, subtract,
    # and cast back to a period
    freq = period.freqstr
    year_ago = period.to_timestamp() - pd.DateOffset(years=1)
    return pd.Period(year_ago, freq=freq)

def subtract_month(period):
    freq = period.freqstr
    month_ago = period.to_timestamp() - pd.DateOffset(months=1)
    return pd.Period(month_ago, freq=freq)


def relative_change(start, end):
    if start == 0:
        return np.nan
    else:
        return (end / start) - 1

def calc_rpt(metric, reporting_period, comparison='yoy'):
    """
    * metric: a Pandas time series giving the values of a metric
    * reporting_period: a Pandas period object indicating the
      period we want to report about.
      
    Returns a Pandas series containing measurements of the metric (currently,
    the value during the reporting period, the year-over-year change, (or 
    the month-over-month change for the content gaps metric) and a naive forecast for the next period).
    """
    # Use get rather than direct indexing so that missing data results in NaNs rather
    # than errors. We want NaNs, not nulls, so that math on them results in NaNs rather
    # than errors.
    value = metric.get(reporting_period, np.nan)
    
    year_ago = subtract_year(reporting_period)
    month_ago = subtract_month(reporting_period)
    year_ago_value = metric.get(year_ago, np.nan)
    month_ago_value = metric.get(month_ago, np.nan)
    
    period_after_year_ago = year_ago + 1
    period_after_year_ago_value = metric.get(period_after_year_ago, np.nan)
    
    naive_forecast = period_after_year_ago_value / year_ago_value * value if year_ago_value != 0 else np.nan

    if comparison == 'yoy':
        return pd.Series({
            "value": value,
            "year_over_year_change": relative_change(year_ago_value, value),
            "naive_forecast": naive_forecast
        })
    
    elif comparison == 'mom':
        return pd.Series({
            "value": value,
            "month_over_month_change": relative_change(month_ago_value, value),
            "naive_forecast": naive_forecast
        })


def format_number(x):
    if isinstance(x, Number) and not pd.isnull(x):
        if x == 0:
            return "0.0%"
        x = sig_figs(x, 3)
        M = 1_000_000
        G = 1_000_000_000
        
        if x < 5:
            return pct_str(x)
        elif x < M:
            return "{:,.0f}".format(x)
        elif x < G:
            x_in_M = sig_figs(x / M, 3)
            return f"{x_in_M} M"
        else:
            x_in_G = sig_figs(x / G, 3)
            # I would like to use G here, but in my experience, people don't
            # necessarily understand what it means but do understand B for "billion".
            return f"{x_in_G} B"
    else:
        return x

def format_report(df, metrics_type, reporting_period):
    """
    * df: a Pandas data frame (intended to be the output of applying calc_rpt to 
      a data frame of metrics)
    * metrics_type: a string identifying the type of metrics in the report
      (e.g. "core", "essential"). Used to add a table header.
    * reporting_period: a Pandas period identifying the period the report is 
      about. Also used in the table header
      
    Returns a formatted and styled data frame useful for
    human reading
    """
    header = f"{reporting_period} {metrics_type} metrics"
    
    new_columns= pd.MultiIndex.from_product([[header], df.columns])
    df.columns = new_columns
    
    df = (
        df
        .applymap(format_number)
        .fillna("–")
        .style
        .set_table_styles([{
            "selector": "th.col_heading.level0",
            "props": "font-size: 1.5em; text-align: center; font-weight: bold;"
        }])
    )
    
    return df


def prepare_date_params(metrics_month_text):
    metrics_month = pd.Period(metrics_month_text)
    mediawiki_history_snapshot = metrics_month_text
    
    date_params = {
        "api_metrics_month_first_day": metrics_month.asfreq("D", how="start").strftime("%Y%m%d"),
        "api_metrics_month_day_after": (metrics_month + 1).asfreq("D", how="start").strftime("%Y%m%d"),
        "mediawiki_history_snapshot": mediawiki_history_snapshot,
        "metrics_cur_month": metrics_month.month,
        "metrics_month": str(metrics_month),
        "metrics_month_first_day": str(metrics_month.asfreq("D", how="start")),
        "metrics_month_end": str((metrics_month + 1).start_time),
        "metrics_month_last_day": str(metrics_month.asfreq("D", how="end")),
        "metrics_month_start": str(metrics_month.start_time),
        "metrics_next_month_first_day": str((metrics_month + 1).asfreq("D", how="start")),
        "metrics_prev_month": str(metrics_month - 1),
        "metrics_year": metrics_month.year,
        "retention_cohort": str(metrics_month - 2)
    }
    return date_params