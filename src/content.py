import pandas as pd
import wmfdata

def quality_article_count(df):
    return df["quality_article_count"].sum()


def calculate_category_totals(df):
    # Create subsets of the data only when the content gaps data is queried. 
    # This step is skipped if total quality articles is being queried.
    try:
        place_articles = df.query("content_gap == 'geography_wmf_region'")
        developed_wp_place_articles = place_articles.query("wiki_status == 'developed'")
        emerging_wp_place_articles = place_articles.query("wiki_status == 'emerging'")
        developed_wp_underrepresented_place_articles = place_articles.query("wiki_status == 'developed' and category_status == 'underrepresented'")
        emerging_wp_underrepresented_place_articles = place_articles.query("wiki_status == 'emerging' and category_status == 'underrepresented'")

        biographies = df.query("content_gap == 'gender'")
        developed_wp_biographies = biographies.query("wiki_status == 'developed'")
        emerging_wp_biographies = biographies.query("wiki_status == 'emerging'")
        developed_wp_underrepresented_biographies = biographies.query("wiki_status == 'developed' and category_status == 'underrepresented'")
        emerging_wp_underrepresented_biographies = biographies.query("wiki_status == 'emerging' and category_status == 'underrepresented'")
    except Exception:
        return df

    # Create the results DataFrame
    try:
        results = pd.DataFrame({
            'month': df["month"].iloc[0],
            'quality articles about places': place_articles.pipe(quality_article_count),
            'quality articles in developed Wikipedias about places': developed_wp_place_articles.pipe(quality_article_count),
            'quality articles in developed Wikipedias about places in underrepresented regions': developed_wp_underrepresented_place_articles.pipe(quality_article_count),
            'quality articles in emerging Wikipedias about places': emerging_wp_place_articles.pipe(quality_article_count),
            'quality articles in emerging Wikipedias about places in underrepresented regions': emerging_wp_underrepresented_place_articles.pipe(quality_article_count),
            'quality biographies': biographies.pipe(quality_article_count),
            'quality biographies in developed Wikipedias': developed_wp_biographies.pipe(quality_article_count),
            'quality biographies in developed Wikipedias of women and gender-diverse people': developed_wp_underrepresented_biographies.pipe(quality_article_count),
            'quality biographies in emerging Wikipedias': emerging_wp_biographies.pipe(quality_article_count),
            'quality biographies in emerging Wikipedias of women and gender-diverse people': emerging_wp_underrepresented_biographies.pipe(quality_article_count)
        }, index=[0])
    except IndexError:
        return df

    return results




def calculate_mom(df):
    # Calculate Month-over-Month difference for the last two rows, prefixing 'new ' to the column names
    for col in df.columns:
        if 'month' not in col and 'new' not in col:
            new_col_name = 'new ' + col
            last_value = df[col].iloc[-1]
            second_last_value = df[col].iloc[-2]
            mom_diff = last_value - second_last_value
            df.loc[df.index[-1], new_col_name] = mom_diff  
    return df


def calc_content_gap_quarterly(metrics, new_index, content_gap_metrics):
    """
    Checks if the last row of the 'total quality articles' 
    column in the DataFrame is NaN and prints an error message if true. Then computes quarterly averages.
    """

    
    if pd.isna(metrics['quality articles'].iloc[-1]):
        wmfdata.utils.print_err("This quarterly report is based on incomplete data, as some months' data is not available.")
        
        # Resample and calculate quarterly averages
        quarterly_averages = (
            metrics
            .reindex(new_index)
            .resample("Q-JUN")
            .aggregate(lambda x: x.mean(skipna=True))
        )  
               
        return quarterly_averages[content_gap_metrics]



















