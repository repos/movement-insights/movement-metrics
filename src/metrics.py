import datetime
import time

import pandas as pd
import requests
from .MetricSet import MetricSet  
import wmfdata as wmf
import src.content as content

from .utils import prepare_date_params


def update_contributor_pipeline_metrics(metrics_month_text):
    contributor_queries = {
        "active_editors": {
            "file": "queries/active_editors.sql"
        },
         "active_editors_region_type": {
            "file": "queries/active_editors_region_type.sql"
        },
        "active_admins": {
            "file": "queries/active_admins.sql"
        },
        "account_registrations": {
            "file": "queries/account_registrations.sql"
        }
    }
    contributor_metrics = MetricSet("contributors", contributor_queries)
    contributor_metrics.run_queries(metrics_month_text)
    contributor_metrics.save_data()

def update_editing_metrics(metrics_month_text):
    editing_queries = {
        "mobile_edits": {
            "file": "queries/mobile_edits.sql"
        },
        "new_editor_retention": {
            "file": "queries/new_editor_retention.sql"
        }
    }
    editing_metrics = MetricSet("editing", editing_queries)
    editing_metrics.run_queries(metrics_month_text)
    editing_metrics.save_data()
    
def update_regional_editor_metrics(metrics_month_text):
    regional_editor_queries = {
        "regional_active_editors": {
            "file": "queries/regional_active_editors.sql"
        }
    }
    regional_editor_metrics = MetricSet("regional_editor_metrics", regional_editor_queries)
    regional_editor_metrics.run_queries(metrics_month_text)
    regional_editor_metrics.save_data()


def update_new_content_metrics(metrics_month_text):
    NEW_PAGES_API = (
        "https://wikimedia.org/api/rest_v1/metrics/"
        "edited-pages/new/{project}/all-editor-types/{page_type}/monthly/{start}/{end}"
    )
    headers = {
        "User-Agent": "https://github.com/wikimedia-research/movement-metrics (bot)"
    }
    
    date_params = prepare_date_params(metrics_month_text)
    start = date_params["api_metrics_month_first_day"]
    end = date_params["api_metrics_month_day_after"]
    
    def get_new_pages(project="all-projects", page_type="content", start = start, end = end):
        url = NEW_PAGES_API.format(project=project, page_type=page_type, start=start, end=end)
        r = requests.get(url, headers=headers)
        if r.status_code == 404:
            return None
        else:
            data = r.json()["items"][0]["results"]
            frame = pd.DataFrame(data)
            frame["timestamp"] = pd.to_datetime(frame["timestamp"]).dt.tz_localize(None)
            frame = frame.rename(columns={"timestamp": "month"}).set_index("month").to_period("M")
            return frame
    
    new_content_metrics = MetricSet("content", '')

    total_new = get_new_pages(start=date_params["api_metrics_month_first_day"], end=date_params["api_metrics_month_day_after"]).rename(columns={"new_pages": "new content pages"})
    new_content_metrics.add_data(total_new)

    wikidata_new = get_new_pages(project="wikidata.org", start=date_params["api_metrics_month_first_day"], end=date_params["api_metrics_month_day_after"]).rename(columns={"new_pages": "new Wikidata entities"})
    new_content_metrics.add_data(wikidata_new)

    commons_new = get_new_pages(project="commons.wikimedia.org", start=date_params["api_metrics_month_first_day"], end=date_params["api_metrics_month_day_after"]).rename(columns=({"new_pages": "new Commons content pages"}))
    new_content_metrics.add_data(commons_new)

    wp_domains = wmf.spark.run("""
        SELECT domain_name
        FROM canonical_data.wikis
        WHERE database_group = "wikipedia"
    """)["domain_name"]

    results = []
    n = len(wp_domains)
    for i, domain in enumerate(wp_domains):
        p = i + 1
        if p % 50 == 0:
            print(f"Now on project {p} of {n} ({domain})")
        frame = get_new_pages(project=domain, start=date_params["api_metrics_month_first_day"], end=date_params["api_metrics_month_day_after"])
        if frame is not None:
            frame["project"] = domain
            results.append(frame)
        time.sleep(0.02)

    if results:
        new_per_wp = pd.concat(results)
        wikipedia_new = new_per_wp.groupby("month").agg({"new_pages": "sum"}).rename(columns=({"new_pages": "new Wikipedia articles"}))
        new_content_metrics.add_data(wikipedia_new)
    else:
        print("No data to aggregate.")
    
    new_content_metrics.save_data()

def update_readers_metrics(metrics_month_text):
    readers_queries = {
        "pageviews": {
            "file": "queries/pageviews.sql"
        },
        "automated_pageviews": {
            "file": "queries/automated_pageviews.sql"
        },
        "page_previews": {
            "file": "queries/page_previews.sql"
        }
    }
    readers_metrics = MetricSet("content_interactions", readers_queries)
    readers_metrics.run_queries(metrics_month_text)
    readers_metrics.data = readers_metrics.data.assign(**{
        'content interactions': lambda df: df["previews"] + df["pageviews"],
        'user pageviews': lambda df: df["pageviews"] - df["automated pageviews"]
    })
    readers_metrics.save_data()

def update_pageviews_referral(metrics_month_text):
    pageviews_referral_query = {
        "pageviews_referral": {
            "file": "queries/pageviews_referral.sql"
        }
    }
    pageviews_referral = MetricSet("referral_source", pageviews_referral_query)
    pageviews_referral.run_queries(metrics_month_text)
    pageviews_referral.save_data()

def update_unique_devices_metrics(metrics_month_text):
    unique_devices_queries = {
        "regional_unique_devices": {
            "file": "queries/regional_unique_devices.sql"
        },
        "unique_devices": {
            "file": "queries/unique_devices.sql"
        }
    }
    regional_unique_devices = MetricSet("unique_devices", unique_devices_queries)
    regional_unique_devices.run_queries(metrics_month_text)
    regional_unique_devices.save_data()

def update_content_gap_metrics(metrics_month_text):
    content_gap_queries = {
        "total_quality_articles": {
            "file": "queries/quality_articles.sql"
        },
        "content_gap": {
           "file": "queries/content_gap.sql"
        }
    }

    content_gap_metrics = MetricSet("content_gaps", content_gap_queries)
    content_gap_metrics.run_queries(metrics_month_text, cleanup_function=content.calculate_category_totals)

    content_gap_metrics.data = content.calculate_mom(content_gap_metrics.data)
    content_gap_metrics.save_data()

def update(metrics_month_text):
   
    functions_to_run = [
        update_contributor_pipeline_metrics,
        update_editing_metrics,
      # update_regional_editor_metrics, -- This function is temporarily disabled and will not be run until the temporary account initiative is fully integrated into all our data pipelines. See: https://phabricator.wikimedia.org/T374148
        update_new_content_metrics,
        update_readers_metrics,
        update_pageviews_referral,
        update_unique_devices_metrics,
        update_content_gap_metrics
    ]

    
    for func in functions_to_run:
        func(metrics_month_text)
